import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css']
})
export class TextComponent implements OnInit {
  @Input()
  text: string;

  constructor() {}

  ngOnInit() {}

  getText(value: string) {
    console.log('getText');
    return value + ' + getText';
  }

  changeText(text: string) {
    console.log('TCL: TextComponent -> changeText -> changeText');
    this.text = text + ' + click';
  }
}
