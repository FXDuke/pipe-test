import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  texts: Array<string> = [];
  constructor() {
    for (let i = 0; i < 1000; i++) {
      this.texts.push('text_' + i);
    }
  }

  ngOnInit() {}
}
