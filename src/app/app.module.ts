import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TooltipPipe } from './tooltip.pipe';
import { TextComponent } from './text/text.component';

@NgModule({
  declarations: [
    AppComponent,
    TooltipPipe,
    TextComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
